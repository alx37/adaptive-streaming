var manifestUri = 'manifest.mpd';
var player;
var videoWidth;
var videoHeight;
var streamBandwidth;
var estimatedBandwidth;
var droppedFrames;
var representationId;
var timestamp;
var metricsTextFile = null;
var adaptationTextFile = null;

var metricsTimer = null;

function initApp() {
  // Install built-in polyfills to patch browser incompatibilities.
  shaka.polyfill.installAll();
  // Check to see if the browser supports the basic APIs Shaka needs.
  if (shaka.Player.isBrowserSupported()) {
    // Everything looks good!
    initPlayer();
  } else {
    // This browser does not have the minimum set of APIs we need.
    console.error('Browser not supported!');
  }

  // Start event listeners that will create the log CSV files for download
  initEventListeners();

  // Initiate a timer that will update metrics every second
  initMetricsLogging();
}

function initPlayer() {
  // Create a Player instance.
  var video = document.getElementById('video');
  player = new shaka.Player(video);

  // Attach player to the window to make it easy to access in the JS console.
  window.player = player;

  // Listen for error events.
  player.addEventListener('error', onErrorEvent);
  player.addEventListener('adaptation', onAdaptationEvent);

  // Try to load a manifest.
  // This is an asynchronous process.
  player.load(manifestUri).then(function() {
      // This runs if the asynchronous load is successful.
      console.log('The video has now been loaded!');
      }).catch(onError);  // onError is executed if the asynchronous load fails.

}

function initMetricsLogging() {
  clearTimeout(metricsTimer);
  var updateMetricsInterval = 1000;
  clearInterval(metricsTimer);
  metricsTimer = setInterval(function () {
      updateMetrics();
      }, updateMetricsInterval)
}

function initEventListeners() {
  var metricsCreate = document.getElementById('metricsCreate');

  metricsCreate.addEventListener('click', function () {
      textbox = document.getElementById('metricsTextbox');
      var link = document.getElementById('metricsDownloadlink');
      link.href = makeMetricsTextFile(textbox.value);
      link.style.display = 'block';
    }, false);

  var adaptationCreate = document.getElementById('adaptationCreate');

  adaptationCreate.addEventListener('click', function () {
      textbox = document.getElementById('adaptationTextbox');
      var link = document.getElementById('adaptationDownloadlink');
      link.href = makeAdaptationTextFile(textbox.value);
      link.style.display = 'block';
    }, false);
}

function makeMetricsTextFile(text) {
  var data = new Blob([text], {type: 'text/plain'});

  // If we are replacing a previously generated file we need to
  // manually revoke the object URL to avoid memory leaks.
  if (metricsTextFile !== null) {
    window.URL.revokeObjectURL(metricsTextFile);
  }

  metricsTextFile = window.URL.createObjectURL(data);

  return metricsTextFile;

};

function makeAdaptationTextFile(text) {
  var data = new Blob([text], {type: 'text/plain'});

  // If we are replacing a previously generated file we need to
  // manually revoke the object URL to avoid memory leaks.
  if (adaptationTextFile !== null) {
    window.URL.revokeObjectURL(adaptationTextFile);
  }

  adaptationTextFile = window.URL.createObjectURL(data);

  return adaptationTextFile;

};

function updateView() {
  document.querySelector("#width").innerHTML = "Width: " + videoWidth;
  document.querySelector("#height").innerHTML = "Height: " + videoHeight;
  document.querySelector("#streamBandwidth").innerHTML = "Stream Bandwidth: " + streamBandwidth;
  document.querySelector("#estimatedBandwidth").innerHTML = "Estimated Bandwidth: " + estimatedBandwidth;
  document.querySelector("#droppedFrames").innerHTML = "Dropped Frames: " + droppedFrames;
  document.querySelector("#representationId").innerHTML = "Representation ID: " + representationId + "/5";
  document.querySelector("#timestamp").innerHTML = "Timestramp of last adaptation: " + new Date(timestamp*1000);
}

function updateMetrics() {
  var stats = player.getStats();

  // Log the update
  console.debug('Updating metrics');

  // Update the local stats
  videoWidth = stats.width;
  videoHeight = stats.height;
  streamBandwidth = stats.streamBandwidth;
  estimatedBandwidth = stats.estimatedBandwidth;
  droppedFrames = stats.droppedFrames;
  representationId = stats.switchHistory[stats.switchHistory.length-1].id;
  timestamp = stats.switchHistory[stats.switchHistory.length-1].timestamp;

  logMetrics(Date.now()/1000 + ', ' + timestamp + ', ' + representationId + ', ' + videoWidth + ', ' + videoHeight + ', ' + streamBandwidth + ', ' + estimatedBandwidth + ', ' + droppedFrames);

  // Update the view
  updateView();
}

function onErrorEvent(event) {
  // Extract the shaka.util.Error object from the event.
  onError(event.detail);
}

function onError(error) {
  // Log the error.
  console.error('Error code', error.code, 'object', error);
}

function onAdaptationEvent(event) {
  // Get stats from the player
  onAdaptation(player.getStats());
}

function onAdaptation(stats) {
  if (stats.switchHistory[stats.switchHistory.length-1].type == "video") {
    // Log the adaptation
    console.log('ADAPTATION HAPPENED!!\n',
        'Width of the video: ', stats.width, '\n',
        'Height of the video: ', stats.height, '\n',
        'Stream Bandwidth: ', stats.streamBandwidth, '\n',
        'Estimated Bandwidth: ', stats.estimatedBandwidth, '\n',
        'Playback rate: ', player.getPlaybackRate(), '\n',
        'Decoded frames: ', stats.decodedFrames, '\n',
        'Dropped frames: ', stats.droppedFrames, '\n',
        'Playing time: ', stats.playTime, '\n',
        'Buffering time: ', stats.bufferingTime, '\n',
        'Last switch:', stats.switchHistory[stats.switchHistory.length-1]);

    // Update the local stats
    videoWidth = stats.width;
    videoHeight = stats.height;
    streamBandwidth = stats.streamBandwidth;
    estimatedBandwidth = stats.estimatedBandwidth;
    droppedFrames = stats.droppedFrames;
    representationId = stats.switchHistory[stats.switchHistory.length-1].id;
    timestamp = stats.switchHistory[stats.switchHistory.length-1].timestamp;

    logAdaptation(timestamp + ', ' + representationId + ', ' + streamBandwidth + ', ' + estimatedBandwidth);

    // Update the view
    updateView();
  }
}

function logMetrics(msg) {
  var metricsTextbox = document.getElementById("metricsTextbox");
  metricsTextbox.innerHTML += msg + "\n";
  metricsTextbox.scrollTop = metricsTextbox.scrollHeight;
}

function logAdaptation(msg) {
  var adaptationTextbox = document.getElementById("adaptationTextbox");
  adaptationTextbox.innerHTML += msg + "\n";
  adaptationTextbox.scrollTop = adaptationTextbox.scrollHeight;
}

// document.addEventListener('DOMContentLoaded', initApp);



$(document).ready(function() {
  console.log("ready");

  $("#experiment").submit(function(event) {
    event.preventDefault();
    var bw_up = $("#bw_up").val();
    var bw_low = $("#bw_low").val();
    var time_up = $("#time_up").val();
    var time_low = $("#time_low").val();

    $.ajax({
      url: '/script/',
      data: {
        bw_up: bw_up,
        bw_low: bw_low,
        time_up: time_up,
        time_low: time_low
      },
    })
    .done(function() {
      // Clean logs and init new player etc.
      console.log("success");
      var metricsTextbox = document.getElementById("metricsTextbox");
      metricsTextbox.innerHTML = "";
      var adaptationTextbox = document.getElementById("adaptationTextbox");
      adaptationTextbox.innerHTML = "";
      initApp();
    })
    .fail(function() {
      console.log("error");
    });
  });
});