#!/bin/bash

high_limit=$1		# in kbits
low_limit=$2		# in kbits
high_bw_time=$3		# in seconds
low_bw_time=$4		# in seconds

output=server_high_"$high_limit"_low_"$low_limit"_hightime_"$high_bw_time"_lowtime_"$low_bw_time".log


# Check that all parameters are given
if [ "$#" -ne 4 ]; then
	echo Wrong number of arguments.
	echo Usage: script "<high_limit>" "<low_limit>" "<high_bw_time>" "<low_bw_time>"
	exit 1
fi

echo High limit:@high_limit, Low limit: $low_limit, High bw uptime: $high_bw_time, Low bw uptime: $low_bw_time > $output

echo $(date +%s) $high_limit >> $output
sudo tc qdisc add dev lo root tbf rate "$high_limit"kbit latency 1ms burst "$high_limit"kbit
sleep $high_bw_time

for i in {1..3};
do
	echo $(date +%s) $low_limit >> $output
	sudo tc qdisc change dev lo root tbf rate "$low_limit"kbit latency 1ms burst "$low_limit"kbit
	sleep $low_bw_time

	echo $(date +%s) $high_limit >> $output
	sudo tc qdisc change dev lo root tbf rate "$high_limit"kbit latency 1ms burst "$high_limit"kbit
	sleep $high_bw_time
done

sudo tc qdisc del dev lo root

echo Done