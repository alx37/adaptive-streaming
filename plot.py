import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as plticker
import matplotlib.dates as da

import os, datetime

f = open('metrics.txt','r')

content = f.readlines()

times = []
bandwidths = []
for line in content:
	if "timestamp" in line:
		continue
	splitted = line.split(', ')
	times.append(datetime.datetime.utcfromtimestamp(int(splitted[0].split('.')[0])))
	bandwidths.append(float(splitted[6]))
	# print (type(datetime.datetime.utcfromtimestamp(int(splitted[0].split('.')[0])).strftime('%Y-%m-%dT%H:%M:%SZ')))

fig, ax = plt.subplots()

plt.plot(np.asarray(times),np.asarray(bandwidths))
plt.gca().xaxis.set_major_formatter(da.DateFormatter("%H:%M:%S"))
plt.gca().xaxis.set_major_locator(da.SecondLocator(interval=(round(len(times)/9))))
labels = plt.gca().get_xticklabels()
plt.setp(labels, rotation=30, fontsize=10)
plt.show()