var bodyParser = require('body-parser')
var fs = require("fs");
var express = require('express');
var app = express();
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
const spawn = require('child_process').spawn

// parse application/json
app.use(bodyParser.json())

var script_running = false;

app.use(express.static('public'));
app.get('/index.html', function (req, res) {
    res.sendFile(__dirname + "/" + "index.html");
})

// FOR TESTING ONLY
app.get('/video/', function (req, res) {
    res.sendFile(__dirname + "/public/" + "video_1280x720_1500k_2.webm");
})

// Start bash script
app.get('/script/', function (req, res) {

	var bw_up = req.query.bw_up;
	var bw_low = req.query.bw_low;
	var time_up = req.query.time_up;
	var time_low = req.query.time_low;

	console.log(bw_up + " " + bw_low+ " " +time_up+ " " +time_low);

	if (script_running){
		console.log("Script is already running");
		res.send("Another script already running\n");
	}
	else{
		const script = spawn('scripts/bw_limit.sh', [bw_up, bw_low, time_up, time_low]);
		// const script = spawn('scripts/test_script.sh', ["900", "800", "1", "1"]);
		console.log("Script started");
		script_running = true;


		script.stdout.on('data', function(data) {
		 	console.log(`stdout: ${data}`);
		});
		script.on('error', function(error){
			console.log('error: ' + error)
		});
		script.on('close', function(){
			console.log("Script finished!");
			script_running = false;
		})
		res.send("OK\n");
	}
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port
    console.log("Example app listening at http://%s:%s", host, port)
})